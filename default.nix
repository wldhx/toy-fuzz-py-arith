let
  pkgs = import (fetchTarball (import ./version.nix)) {
    overlays = [ (import ./overlay.nix) ];
  };

in
  pkgs.haskellPackages.callPackage ./arith-fuzzer.nix { }
