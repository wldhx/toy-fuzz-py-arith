self: super: {
  haskellPackages = super.haskellPackages.override (oldArgs: {
    overrides =
      self.lib.composeExtensions (oldArgs.overrides or (_: _: {}))
        (hself: hsuper: {
          bytestring-show = self.haskell.lib.doJailbreak hsuper.bytestring-show;
        });
  });
}

