{ mkDerivation, async, base, bytestring, bytestring-show, hashable
, MonadRandom, mtl, QuickCheck, say, stdenv, text, time
, typed-process, unordered-containers
}:
mkDerivation {
  pname = "arith-fuzzer";
  version = "0.0.0.1";
  src = ./.;
  isLibrary = false;
  isExecutable = true;
  executableHaskellDepends = [
    async base bytestring bytestring-show hashable MonadRandom mtl
    QuickCheck say text time typed-process unordered-containers
  ];
  license = stdenv.lib.licenses.agpl3;
}
