# toy-py-arith-fuzzer

A parallel Py3 arith expr performance fuzzer.

Generates exprs such that the interpreter will take longer to evaluate them. Outputs metrics at each step: `(worker id, eval time, (serialized expr length, num lits count, max num in expr))`.

## Expr gen algorithm

- Initial expr is random
- At each step, toss a weighted coin to either increase each num lit in expr or add an op
- Adjust weights by time difference that made
- Repeat maxIters times

So, ascent on 𝜏(ε(w))!

## Usage

Consult CI (`.gitlab-ci.yml`) for build instructions.

```
cabal new-run arith-fuzzer -- 10        1
                              maxIters  jobs
```

## Impl notes

- Each thread keeps its own interpreter throughout its life.
- Exprs are only allowed to grow.
- Only `+` op supported at the moment; adding others is a three-line change.
- One cannot `hpython` if one cannot `lens`; so, trivial AST hand-rolled.
- On large exprs, CPython will `RecursionError` out. This is not handled.

## Future considerations

- Consider shrinking exprs (QuickCheck)
- Consider robust timing (Criterion)
- Use Python-measured time
- c.f. <https://github.com/mathandley/AutoBench>, <https://github.com/qfpl/hpython/tree/develop/hpython-test>
- Consider symmetries
- Synchronize threads
