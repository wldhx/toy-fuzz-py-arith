{
  name = "nixpkgs-release-20.03-2020-04-05";
  url = "https://github.com/nixos/nixpkgs/archive/92e20dc020992b0a50e09eddf9688cc5c72e461d.tar.gz";
  sha256 = "1sgca80j99v8f2vv4s0acq0jvmmpjyq8hlgv2d3i13bq0dzjdkpw";
}
