{-# LANGUAGE FlexibleInstances #-}

module Arith where
import           Control.Monad
import qualified Data.ByteString.Char8         as C
import qualified Data.ByteString.Lazy.Char8    as LC
import           Test.QuickCheck
import qualified Text.Show.ByteString          as B

data AST a = NumLit a | Plus (AST a) (AST a) | Mul (AST a) (AST a) deriving (Functor, Foldable, Traversable)

instance B.Show a => Show (AST a) where
  show = C.unpack . LC.toStrict . render

render :: B.Show a => AST a -> LC.ByteString
render (NumLit a) = B.show a
render (Plus a b) = render a <> "+" <> render b
render (Mul  a b) = render a <> "*" <> render b

-- http://www.cse.chalmers.se/~rjmh/QuickCheck/manual.html
instance Arbitrary (AST Int) where
  arbitrary = sized tree'
   where
    tree' 0         = fmap NumLit getPositive <$> arbitrary
    tree' n | n > 0 = oneof
      [fmap NumLit getPositive <$> arbitrary, liftM2 Plus subtree subtree]
      where subtree = tree' (n `div` 2)
