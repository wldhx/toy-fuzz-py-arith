module Main where
import           Control.Concurrent.Async
import           Control.Monad
import           Control.Monad.Except
import qualified Control.Monad.Random.Class    as MR
import qualified Data.ByteString.Char8         as C
import qualified Data.ByteString.Lazy.Char8    as LC
import           Data.Bifunctor                 ( second )
import           Data.Either                    ( fromLeft )
import qualified Data.HashMap.Strict           as HM
import           Data.Text                      ( Text )
import           Say                            ( say
                                                , sayShow
                                                )
import           System.Environment             ( getArgs )
import           System.Exit                    ( ExitCode(ExitSuccess) )
import           System.IO                      ( hFlush )
import           System.Process.Typed
import           Test.QuickCheck
import           Text.Read                      ( read )
import           Action
import           Arith
import           Util

initAW :: HM.HashMap Action Double
initAW = HM.fromList [(IncreaseNums, 1), (AddPlus, 1)]

work :: Show a => Int -> a -> IO ()
work maxIters id_ = do
  let conf =
        setStdin createPipe $ setStdout createPipe $ setStderr inherit $ proc
          "python3"
          ["-Iiu", "-ddd", "-c", "import sys;sys.ps1='';sys.ps2=''"]
  withProcessWait conf $ \p -> do
    let tell cmd = C.hPutStrLn (getStdin p) cmd >> hFlush (getStdin p)
    let test cmd = timer (tell cmd) (void . C.hGetLine $ getStdout p)
    let testExpr = test . LC.toStrict . render

    tell "'warmup'"
    let cb :: Int -> AST Int -> HM.HashMap Action Double -> IO (Either Text ())
        cb cnt expr w = runExceptT $ do
          action <- MR.fromList $ map (second toRational) $ HM.toList w
          t0     <- liftIO $ realToFrac <$> testExpr expr
          let (transform, h) = eval action
          let expr'          = transform expr
          t1 <- liftIO $ realToFrac <$> testExpr expr'
          let drv = (relu $ log t1 - log t0) / (2 * h)
          liftIO . sayShow $ (id_, t1, metrics expr')
          let w' = HM.adjust (+drv) action w
          guard (cnt < maxIters)
          r <- liftIO $ cb (cnt + 1) expr' w'
          case r of
            Left  e  -> throwError e
            Right () -> pure ()
    start_expr <- generate arbitrary
    say . fromLeft undefined =<< cb 1 start_expr initAW

    tell "sys.exit(0)"
    exitCode <- waitExitCode p
    when (exitCode /= ExitSuccess) $ say ":("

main :: IO ()
main = do
  [maxIters, jobs] <- map (read @Int) <$> getArgs
  work maxIters `mapConcurrently_` [1 .. jobs]
