module Action where
import           Data.Hashable                  ( Hashable )
import           GHC.Generics                   ( Generic )

import           Arith


data Action = IncreaseNums | AddPlus deriving (Show, Eq, Generic)
instance Hashable Action

eval :: Num a => Action -> (AST Int -> AST Int, a)
eval IncreaseNums = (fmap (+ 100), 100)
eval AddPlus      = (Plus (NumLit 1), 1)
