module Util where
import qualified Data.ByteString.Lazy.Char8    as LC
import           Data.Foldable
import           Data.Time

import           GHC.Int                        ( Int64 )

import           Arith

timer :: IO () -> IO () -> IO NominalDiffTime
timer setup act = do
  setup
  start <- getCurrentTime
  act
  end <- getCurrentTime
  pure (diffUTCTime end start)

metrics :: AST Int -> (GHC.Int.Int64, Integer, Int)
metrics e = (LC.length . render $ e, foldr' (const (+ 1)) 0 e, maximum e)

relu :: (Ord a, Num a) => a -> a
relu = max 0
